<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'productName' => $this->product->name,
            'productQty' => $this->quantity,
            'productPrice' => '$' . ($this->price / 100),
            'discount' => $this->discount . '%',
            'productSum' => '$' . ($this->sum / 100)
        ];
    }
}

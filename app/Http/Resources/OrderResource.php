<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $items = $this->orderItems()->get();
        return [
            'data' => [
            'orderId' => $this->id,
            'orderDate' => date("d-m-Y", strtotime($this->created_at)),
            'orderSum' => '$' . ($items->sum('sum') / 100),
            'orderItems' => $items->map(function ($item){
                return new OrderItemResource($item);
            })],
            'buyer' => new BuyerResource($this->buyer)
        ];
    }
}
